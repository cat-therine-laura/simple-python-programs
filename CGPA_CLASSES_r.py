class BasicCalculator(Calculator):
    @staticmethod
    def basic_calculator(operator, values):
        total = 0
        if operator == "+":
            total = sum(values)
        elif operator == "-":
            result = values[0]
            for value in values[1:]:
                result -= value
            total = result
        elif operator == "*":
            result = values[0]
            for value in values[1:]:
                result *= value
            total = result
        elif operator == "/":
            result = values[0]
            for value in values[1:]:
                if value != 0:
                    result /= value
            total = result
        else:
            print("Invalid operation")
        return total
        
class Calculator:
    @staticmethod
    def get_grades(values):
        return [
        "A+" if mark >= 90 else
        "A" if mark >= 80 else
        "B+" if mark >= 75 else
        "B" if mark >= 70 else
        "C+" if mark >= 65 else
        "C" if mark >= 60 else
        "D+" if mark >= 55 else
        "D" if mark >= 50 else
        "E" if mark >= 45 else
        "E-" if mark >= 40 else
        "F"
        for mark in values
    ]



class CGPA(Calculator):
    @staticmethod
    def calculate_cgpa(values, grades, credit_units):
        grade_point = {
            "A+": 5.0,
            "A": 5.0,
            "B+": 4.5,
            "B": 4.0,
            "C+": 3.5,
            "C": 3.0,
            "D+": 2.5,
            "D": 2,
            "E": 1.5,
            "E-": 1.0,
            "F": 0.0
        }
        grade_points = [grade_point[grade] for grade in grades]
        numerators = [BasicCalculator.basic_calculator("*", [gp, cu]) for gp, cu in zip(grade_points, credit_units)]
        numerator = BasicCalculator.basic_calculator("+", numerators)
        denominator = BasicCalculator.basic_calculator("+", credit_units)

        CGPA = BasicCalculator.basic_calculator("/", [numerator, denominator])

        return round(CGPA, 2)

studentNumbers = [1900717775, 2300715495, 2300717998]

sumStudentNumbers = str(BasicCalculator.basic_calculator("+", studentNumbers))
sumStudentNumbers = sumStudentNumbers[1:]
CU1 = int(sumStudentNumbers[0:2])
CU2 = int(sumStudentNumbers[2:4])
CU3 = int(sumStudentNumbers[4:6])
CU4 = int(sumStudentNumbers[6:8])
coursevalues = [CU1, CU2, CU3, CU4]
print(coursevalues)
CGPA_result = CGPA.calculate_cgpa(coursevalues, Calculator.get_grades(coursevalues), [4, 4, 4, 4])

with open("group25CGPA.doc",'a') as word_file:
    word_file.write("\nCGPA using classes.")
    word_file.write(f"\nThe CGPA is: {CGPA_result}")

word_file.close()
'''
OKOT REAGAN            1900717775
NASSALI CATHERINE LAURA    2300715495
TUGUME FAROUK            2300717998
'''
