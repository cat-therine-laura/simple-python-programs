period = int(input("Enter the period in minutes: \n"))
h = period % (24 * 60)
days = (period - h) // (24 * 60)
days = int(days)

minutes = h % 60
hours = (h - minutes) // 60
hours = int(hours)

if days == 0 and hours == 0:
    print(f"{minutes} minutes")
elif days == 0:
    print(f"{hours} hours {minutes} minutes")
else:
    print(f"{days} days {hours} hours {minutes} minutes")
